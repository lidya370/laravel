<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Http\Request;

class LaravelController extends Controller
{
    public function signinForm()
    {
        return view('signin.login');
    }
    public function attempt(Request $request)
    {
        $this->validate($request, [
            'email' => 'email|exists:users,email',
            'password' => 'required',
        ]);
        $attempts = [
            'email' => $request->email,
            'password' => $request->password,
        ];
        if (Auth::attempt($attempts, (bool) $request->remember)) {
            return redirect()->intended('/home');
        }
        return redirect()->back();
    }

    public function SignupForm()
    {
        return view('signup.form');
    }
    public function store(Request $request)
    {
        // validate request data
        $this->validate($request, [
            'name' => 'required|string|max:50',
            'email' => 'required|email|max:100|unique:users,email',
            'password' => 'required|min:6',
            'confirm_password' => 'required|same:password'
        ]);
        // save into table
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
        // autologin
        Auth::loginUsingId($user->id);
        // redirect to home
        return redirect('/home');
    }

    public function home()
    {
        if (Auth::user() == NULL) {
            return redirect('/signin');
        } else {
            return view('home2');
        }
        
        
    }

    public function logout()
 	{
	    Auth::logout();
	    return redirect('/signin');
	}
}
